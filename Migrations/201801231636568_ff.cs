namespace CSRating.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ff : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "TelegramUserId", c => c.Int(nullable: false));
            DropColumn("dbo.Records", "TelegramUserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Records", "TelegramUserId", c => c.Int(nullable: false));
            DropColumn("dbo.Students", "TelegramUserId");
        }
    }
}
