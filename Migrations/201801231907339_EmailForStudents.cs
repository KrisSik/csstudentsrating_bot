namespace CSRating.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmailForStudents : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "EMail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Students", "EMail");
        }
    }
}
