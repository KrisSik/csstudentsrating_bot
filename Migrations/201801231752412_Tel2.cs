namespace CSRating.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tel2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Records", "TelegramUserId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Records", "TelegramUserId");
        }
    }
}
