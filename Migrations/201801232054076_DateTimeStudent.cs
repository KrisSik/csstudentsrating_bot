namespace CSRating.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateTimeStudent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "LastTimeSet", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Students", "LastTimeSet");
        }
    }
}
