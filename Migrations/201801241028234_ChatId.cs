namespace CSRating.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChatId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "TelegramChatId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Students", "TelegramChatId");
        }
    }
}
