﻿using CSRating.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSRating.EF
{
    public class RatingContext : DbContext
    {
        public RatingContext()
            :base("name=connectionString")
        {

        }
        public DbSet<Record> Records { get; set; }
        public DbSet<Student> Students { get; set; }
    }
}
