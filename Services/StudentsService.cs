﻿using CSRating.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace CSRating.Services
{
    public static class StudentsService
    {
        public static int CheckPassword(int userId, string password, int chatId)
        {
            int studentId = -1;
            Student student = Bot.DataBase.Students.FirstOrDefault(s => s.Password == password);
            if (student != null)
            {
                studentId = student.StudentId;
                student.TelegramUserId = userId;
                student.TelegramChatId = chatId; 
                Bot.DataBase.Entry<Student>(student).State = System.Data.Entity.EntityState.Modified;

                Bot.DataBase.SaveChanges();

                LogService.LogToFileAndConsole(string.Format("{0} logged in from {1}", GetStudentFullNameByUserId(userId), userId));
            }
            return studentId;
        }
        public static string GetStudentFirstNameByUserId(int userId)
        {
            Student student = Bot.DataBase.Students.First(s => s.TelegramUserId == userId);
            return student.FirstName;
        }
        public static string GetStudentFullNameByUserId(int userId)
        {
            Student student = Bot.DataBase.Students.First(s => s.TelegramUserId == userId);
            return student.FirstName + " " + student.LastName;
        }
        public static int GetStudentIdByUserId(int userId)
        {
            Student student = Bot.DataBase.Students.First(s => s.TelegramUserId == userId);
            return student.StudentId;
        }
        public static bool IsUserInDatabase(int userId)
        {
            return Bot.DataBase.Students.Any(s => s.TelegramUserId == userId);
        }
        public static int TimeToSetMarks(int userId)
        {
            DateTime date1 = Bot.DataBase.Students.First(s => s.TelegramUserId == userId).LastTimeSet;
            TimeSpan difference = DateTime.Now - date1;
            if (difference.TotalMinutes >= 60)
            {
                return 0;
            } 
            else
            {
                return 60 - (int) difference.TotalMinutes;
            }
        }
        public static int[] GetAllChatIds()
        {
            return Bot.DataBase.Students.Select(s => s.TelegramChatId).ToArray();
        }
        public static async Task SendMessageAsync(TelegramBotClient client, string message)
        {
            int[] ids = GetAllChatIds();
            foreach (int id in ids)
            {
                if (id != 0)
                {
                    await client.SendTextMessageAsync(id, message);
                }
            }
        }
        public static async Task SendKrisPhotoAsync(int chatId, TelegramBotClient client)
        {
            await client.SendPhotoAsync(chatId, new Telegram.Bot.Types.FileToSend("Fuck you.jpg", new FileStream(@"C:\kris.jpg", FileMode.Open)));
        }
    }
}
