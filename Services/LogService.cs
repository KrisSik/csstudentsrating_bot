﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace CSRating.Services
{
    public static class LogService
    {
        private static string FileName { get; set; } = @"C:\rating.log";
        public static void LogToFile(string message)
        {
            try
            {
                StreamWriter file = new StreamWriter(FileName, true);
                file.WriteLine(string.Format("[{0}] {1}", DateTime.Now, message));
                file.Close();
            } 
            catch(Exception e)
            {
                Console.WriteLine("LogService error.");
            }
        }
        public static void LogToConsole(string message)
        {
            Console.WriteLine(string.Format("[{0}] {1}", DateTime.Now, message));
        }
        public static void LogToFileAndConsole(string message)
        {
            LogToConsole(message);
            LogToFile(message);
        }
        public static async Task SendLog(int chatId, TelegramBotClient client, int countOfLines)
        {
            string log = string.Empty;
            try
            {
                IEnumerable<string> lines = File.ReadLines(FileName);
                int count = lines.Count();
                for (int i = countOfLines; i >= 0; i--)
                {
                    if (count - i <= 0)
                    {
                        continue;
                    }
                    log += lines.ElementAt(count - i) + "\n";
                }
            }
            catch(Exception e)
            {
            }
            await client.SendTextMessageAsync(chatId, log);
        }
    }
}
