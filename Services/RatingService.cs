﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace CSRating.Services
{
    public static class RatingService
    {
        public static async Task SendRatingAsync(TelegramBotClient client)
        {
            int[] ids = StudentsService.GetAllChatIds();
            foreach(int id in ids)
            {
                if (id != 0)
                {
                    await client.SendDocumentAsync(id, new Telegram.Bot.Types.FileToSend("Rating.xlsx", new FileStream(@"C:\Rating.xlsx", FileMode.Open)));
                }
            }
        }
        public static async Task CountOfStudents(int chatId, ITelegramBotClient client)
        {
            string message = String.Empty;
            int allcount = Bot.DataBase.Students.Count();
            int count = Bot.DataBase.Records.Select(r => r.StudentId).Distinct().Count();
            if (count % 10 == 1 && ((count / 10) % 10) != 1)
            {
                message = count.ToString() + " студент ввів свої бали.";
            } else
            if ((count % 10 == 2 || count % 10 == 3 || count % 10 == 4) && ((count / 10) % 10) == 0)
            {
                message = count.ToString() + " студенти ввели свої бали.";
            } else
            {
                message = count.ToString() + " студентів ввели свої бали.";
            }
            message +=  "\nЗалишилося " + Math.Round((1 - (float) count/allcount)*100, 2).ToString() + "%.";
            await client.SendTextMessageAsync(chatId, message);
        }
    }
}
