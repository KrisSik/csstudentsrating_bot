﻿using CSRating.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace CSRating.Services
{
    public static class SubjectService
    {
        public static async Task AskMarkForSubjectAsync(Message message, TelegramBotClient client)
        {
            var chatId = message.Chat.Id;
            var userId = message.From.Id;
            if (!message.Text.Contains("setmarks"))
            {
                int mark;
                if (int.TryParse(message.Text, out mark))
                {
                    if (mark > 100 || mark <= 0)
                    {
                        await client.SendTextMessageAsync(chatId, "Введи число від 1 до 100, ДУРАК. (с) Мазур.");
                    }
                    else
                    {
                        Record record = Bot.DataBase.Records.First(r => r.TelegramUserId == userId && r.RecordId == Bot.DataBase.Records.Where(s => s.StudentId == r.StudentId).Max(s => s.RecordId));
                        record.Marks[Bot.UsersSettingMarks[userId].First()] = mark;
                        Bot.DataBase.SaveChanges();

                        Bot.UsersSettingMarks[userId].RemoveAt(0);
                        if (Bot.UsersSettingMarks[userId].Count == 0)
                        {
                            Bot.DataBase.Students.First(s => s.TelegramUserId == userId).LastTimeSet = DateTime.Now;
                            record.RatingMark = CalculateRatingMark(record);
                            Bot.DataBase.SaveChanges();
                            Bot.UsersSettingMarks.Remove(userId);


                            string msg = String.Empty;

                            for(int i = 1; i <= 7; i++)
                            {
                                msg += string.Format("\n{0}: {1}", Bot.Subjects[i], record.Marks[i]);
                            }

                            LogService.LogToFileAndConsole(string.Format("{0} set marks.", StudentsService.GetStudentFullNameByUserId(userId)));

                            await client.SendTextMessageAsync(chatId, string.Format("Дякую. Перевір, чи правильно ввів свої бали:{0}\nТвій рейтинговий бал — {1}. \nЯкщо помилився при введенні — знову скористайся командою /setmarks. \nЯ надішлю тобі таблицю результатів одразу після того, як напишеться останній екзамен. Успіхів!", msg, record.RatingMark.ToString()));
                            return;
                        }
                    }
                } else
                {
                    await client.SendTextMessageAsync(chatId, "Введи число від 1 до 100, ДУРАК. (с) Мазур.");
                }
            }
            await client.SendTextMessageAsync(chatId, string.Format("Скільки балів маєш за {0}?", Bot.Subjects[Bot.UsersSettingMarks[userId].First()]));
        }
        private static float CalculateRatingMark(Record record)
        {
            float sum = 0;
            for (int i = 1; i <=7; i++)
            {
                sum += Bot.Coeficients[i] * record.Marks[i];
            }
            return (float) Math.Round(sum / Bot.Coeficients.Values.Sum(), 3);
        }
    }
}
