﻿using CSRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace CSRating.Commands
{
    class SendRatingCommand : Command
    {
        public override string Name => "sendrating";

        public override async Task ExecuteAsync(Message message, TelegramBotClient client)
        {
            //326373886 - KrisSik id
            if (message.From.Id == 326373886)
            {
                await RatingService.SendRatingAsync(client);
            } else
            {
                await client.SendTextMessageAsync(message.Chat.Id, "Повівся, дурачок))");
            }
        }
    }
}
