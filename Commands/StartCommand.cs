﻿using CSRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace CSRating.Commands
{
    public class StartCommand : Command
    {
        public override string Name => "start";

        public override async Task ExecuteAsync(Message message, TelegramBotClient client)
        {
            var chatId = message.Chat.Id;
            var userId = message.From.Id;
            if (StudentsService.IsUserInDatabase(userId))
            {
                await client.SendTextMessageAsync(chatId, string.Format("Привіт, {0}", StudentsService.GetStudentFirstNameByUserId(userId)));
            } else
            {
                await client.SendTextMessageAsync(chatId, "Привіт. Введи, будь ласка, свій пароль (я надіслав тобі його на пошту, яка вказана в документі Файтаса).");
                if (!Bot.UsersSettingKey.Contains(userId))
                {
                    Bot.UsersSettingKey.Add(userId);
                }
            }
        }
    }
}
