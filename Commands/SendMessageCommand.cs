﻿using CSRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace CSRating.Commands
{
    class SendMessageCommand : Command
    {
        public override string Name => "sendmessage";

        public override async Task ExecuteAsync(Message message, TelegramBotClient client)
        {
            //326373886 - KrisSik id
            if (message.From.Id == 326373886)
            {
                string msg = message.Text.Substring(message.Text.IndexOf(' ') + 1, message.Text.Length - message.Text.IndexOf(' ')-1);
                await StudentsService.SendMessageAsync(client, msg);
            }
        }
    }
}
