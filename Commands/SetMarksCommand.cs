﻿using CSRating.Models;
using CSRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace CSRating.Commands
{
    public class SetMarksCommand : Command
    {
        public override string Name => "setmarks";
        public override async Task ExecuteAsync(Message message, TelegramBotClient client)
        {
            if (StudentsService.IsUserInDatabase(message.From.Id))
            {
                int time = StudentsService.TimeToSetMarks(message.From.Id);
                if (time != 0)
                {
                    await client.SendTextMessageAsync(message.Chat.Id, string.Format("Ти тільки що виставляв оцінки. Зачекай {0} хв.", time));
                    return;
                }
                Bot.UsersSettingMarks.Add(message.From.Id, new List<int>() { 1, 2, 3, 4, 5, 6, 7 });

                Bot.DataBase.Records.Add(new Record() { StudentId = StudentsService.GetStudentIdByUserId(message.From.Id), TelegramUserId = message.From.Id });
                Bot.DataBase.SaveChanges();
                
                await SubjectService.AskMarkForSubjectAsync(message, client);
            } else
            {
                if (!Bot.UsersSettingKey.Contains(message.From.Id))
                {
                    Bot.UsersSettingKey.Add(message.From.Id);
                }
            }
        }
    }
}
