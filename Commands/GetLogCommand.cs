﻿using CSRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace CSRating.Commands
{
    class GetLogCommand : Command
    {
        public override string Name => "getlog";

        public override async Task ExecuteAsync(Message message, TelegramBotClient client)
        {
            int countOfLines = 30;
            if (message.Text.Split(' ').Count() != 1)
            {
                message.Text.Split(' ').First(c => int.TryParse(c, out countOfLines));
            }
            await LogService.SendLog((int) message.Chat.Id, client, countOfLines);
        }
    }
}
