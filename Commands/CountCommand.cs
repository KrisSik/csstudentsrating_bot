﻿using CSRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace CSRating.Commands
{
    public class CountCommand : Command
    {
        public override string Name => "countofstudents";

        public override async Task ExecuteAsync(Message message, TelegramBotClient client)
        {
            await RatingService.CountOfStudents((int) message.Chat.Id, client);
        }
    }
}
