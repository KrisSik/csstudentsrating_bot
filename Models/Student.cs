﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSRating.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        public int TelegramUserId { get; set; }
        public int TelegramChatId { get; set; }
        public string EMail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public DateTime LastTimeSet { get; set; }
    }
}
