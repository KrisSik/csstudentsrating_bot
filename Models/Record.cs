﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSRating.Models
{
    public class Record
    {
        public int RecordId { get; set; }
        public int TelegramUserId { get; set; }
        public int StudentId { get; set; }
        //(int, int) = > (subjectId, mark)
        public Dictionary<int, int> Marks { get; set; }
        public float RatingMark { get; set; }
        public Record()
        {
            Marks = new Dictionary<int, int>();
            for (int i = 1; i <= 7; i++)
            {
                Marks.Add(i, 0);
            }
        }
    }
}
