﻿using CSRating.Commands;
using CSRating.EF;
using CSRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace CSRating
{
    class Program
    {
        static void Main(string[] args)
        {
            Bot.Get().OnMessage += Bot_OnMessageAsync;
            Bot.Get().StartReceiving();
            Console.ReadLine();
            Bot.Get().StopReceiving();
        }

        private static async void Bot_OnMessageAsync(object sender, MessageEventArgs e)
        {
            if (Bot.UsersSettingKey.Contains(e.Message.From.Id))
            {
                int studentId = StudentsService.CheckPassword(e.Message.From.Id, e.Message.Text, (int) e.Message.Chat.Id);
                if (studentId == -1)
                {
                    await Bot.Get().SendTextMessageAsync(e.Message.Chat.Id, string.Format("Такого студента не знайдено. \nПеревір пароль."));
                    return;
                }
                Bot.UsersSettingKey.Remove(e.Message.From.Id);
                await Bot.Get().SendTextMessageAsync(e.Message.Chat.Id, string.Format("Привіт, {0}", StudentsService.GetStudentFirstNameByUserId((int) e.Message.Chat.Id)));
                return;
            }
            if (Bot.UsersSettingMarks.ContainsKey(e.Message.From.Id))
            {
                await SubjectService.AskMarkForSubjectAsync(e.Message, Bot.Get());
                return;
            }
            foreach (var command in Bot.Commands)
            {
                if (command.Contains(e.Message.Text))
                {
                    await command.ExecuteAsync(e.Message, Bot.Get());
                    break;
                }
            }
        }
    }
}
