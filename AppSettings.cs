﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSRating
{
    public static class AppSettings
    {
        public static string Name { get; set; } = "CsStudentsRating_bot";
        public static string Api_Key { get; set; } = "*******************";
    }
}