﻿using CSRating.Commands;
using CSRating.EF;
using CSRating.Models;
using CSRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace CSRating
{
    public static class Bot
    {
        private static TelegramBotClient client;
        public static List<Command> Commands = new List<Command>();
        //(int, int) = > (userId, list of subjectId's he must set)
        public static Dictionary<int, List<int>> UsersSettingMarks = new Dictionary<int, List<int>>();
        //(int, string) = > (id of subject, user name)
        public static List<int> UsersSettingKey = new List<int>();
        //(int, string) = > (id of subject, name of subject)
        public static Dictionary<int, string> Subjects = new Dictionary<int, string>();
        //(int, string) = > (id of subject, coeficient)
        public static Dictionary<int, int> Coeficients = new Dictionary<int, int>();
        public static RatingContext DataBase;
        static Bot()
        {
            Subjects.Add(1, "Комп'ютерні мережі");
            Subjects.Add(2, "Технології комп'ютерного проектування");
            Subjects.Add(3, "Моделювання систем");
            Subjects.Add(4, "Теоретичні основи САПР");
            Subjects.Add(5, "Технології створення програмного продукту");
            Subjects.Add(6, "Інтелектуальний аналіз даних");
            Subjects.Add(7, "Методи синтезу та оптимізації");

            Coeficients.Add(1, 4);
            Coeficients.Add(2, 5);
            Coeficients.Add(3, 4);
            Coeficients.Add(4, 5);
            Coeficients.Add(5, 5);
            Coeficients.Add(6, 4);
            Coeficients.Add(7, 3);

            Commands.Add(new SetMarksCommand());
            Commands.Add(new StartCommand());
            Commands.Add(new SendRatingCommand());
            Commands.Add(new GetLogCommand());
            Commands.Add(new CountCommand());
            Commands.Add(new GetKrisPhotoCommand());
            Commands.Add(new SendMessageCommand());


            DataBase = new RatingContext();
            LogService.LogToFileAndConsole("\n\nBot started.");
        }
        public static TelegramBotClient Get()
        {
            if (client != null)
            {
                return client;
            }
            client = new TelegramBotClient(AppSettings.Api_Key);
            return client;
        }
    }
}
